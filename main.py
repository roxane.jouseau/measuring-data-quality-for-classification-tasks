import pandas as pd
import data_quality_metric as dqm
from multiprocessing import freeze_support

iris_df = pd.read_csv("dataset/iris/iris.csv")

models = ['logistic regression', 'knn', 'decision tree', 'random forest', 'ada boost', 'naive bayes', 'xgboost',
          'svc', 'gaussian process', 'mlp', 'sgd', 'gradient boosting']
crt_names = ['missing', 'fuzzing', 'outlier']

if __name__ == '__main__':
    freeze_support()
    dqm.dq_metric_para(30, iris_df, crt_names, models, 'iris')  # parallel on the 30 resamplings
